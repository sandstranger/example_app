package com.example.app.main;

import android.app.Application;

import com.example.app.client.SugarORMClient;

public class WorkersApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SugarORMClient.initSugar(this);
    }

    @Override
    public void onTerminate(){
        super.onTerminate();
        SugarORMClient.terminateSugar();
    }
}
