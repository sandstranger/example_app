package com.example.app.interfaces.components;

import com.example.app.interfaces.qualifiers.AppCompatActivityProvider;
import com.example.app.modules.AppCompatActivityModule;
import com.example.app.view.activity.MainActivity;

import dagger.Component;

@Component (modules = AppCompatActivityModule.class)
public interface AppCompatActivityComponent {
    @AppCompatActivityProvider
    MainActivity getAppCompatActivity();
}
