package com.example.app.interfaces.components;

import com.example.app.client.WorkersClient;
import com.example.app.interfaces.api.WorkersApi;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = com.example.app.modules.WorkersClientModule.class)
@Singleton
public interface WorkersClientComponent {
    void inject(WorkersClient client);
    WorkersApi getWorkersApi();
}
