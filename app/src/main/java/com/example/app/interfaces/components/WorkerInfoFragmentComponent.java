package com.example.app.interfaces.components;

import com.example.app.modules.WorkerInfoFragmentModule;
import com.example.app.view.fragment.WorkerInfoFragment;

import javax.inject.Singleton;

import dagger.Component;

@Component (modules = WorkerInfoFragmentModule.class)
@Singleton
public interface WorkerInfoFragmentComponent {
    void inject (WorkerInfoFragment fragment);
}
