package com.example.app.interfaces.components;

import com.example.app.modules.FragmentsControllerModule;
import com.example.app.view.fragment.SpecialtyFragment;
import com.example.app.view.fragment.WorkerInfoFragment;
import com.example.app.view.fragment.WorkersInfoFragment;

import dagger.Component;

@Component(modules = FragmentsControllerModule.class)
public interface FragmentsControllerComponent {
    WorkersInfoFragment getWorkersInfoFragment();

    WorkerInfoFragment getWorkerInfoFragment();

    SpecialtyFragment getSpecialtyFragment();
}
