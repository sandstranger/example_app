package com.example.app.interfaces.listeners;

public interface Callback<T> {
    default void callAction(){

    };

    default void callAction(T data) {

    }
}
