package com.example.app.interfaces.api;

import com.example.app.model.Worker;
import com.example.app.model.WorkersList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

import static com.example.app.utils.Constants.WORKERS_JSON_FILE_NAME;

public interface WorkersApi {
    @GET(WORKERS_JSON_FILE_NAME)
    Call<WorkersList> getWorkers();
}
