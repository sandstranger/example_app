package com.example.app.interfaces.components;

import com.example.app.modules.SpecialtyViewHolderModule;
import com.example.app.view.viewholder.SpecialtyViewHolder;

import javax.inject.Singleton;

import dagger.Component;

@Component (modules = SpecialtyViewHolderModule.class)
@Singleton
public interface SpecialtyViewHolderComponent {
    void inject (SpecialtyViewHolder viewHolder);
}
