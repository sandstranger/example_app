package com.example.app.interfaces.components;

import com.example.app.modules.WorkersInfoFragmentModule;
import com.example.app.view.fragment.WorkersInfoFragment;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = WorkersInfoFragmentModule.class)
@Singleton
public interface WorkersInfoFragmentComponent {
    void inject (WorkersInfoFragment fragment);
}
