package com.example.app.interfaces.listeners;

public interface OnBackPressed {
    void onBackPressed();
}
