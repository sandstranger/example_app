package com.example.app.interfaces.components;

import com.example.app.modules.SpecialtyFragmentModule;
import com.example.app.view.fragment.SpecialtyFragment;

import javax.inject.Singleton;

import dagger.Component;

@Component (modules = SpecialtyFragmentModule.class)
@Singleton
public interface SpecialtyFragmentComponent {
    void inject (SpecialtyFragment fragment);
}
