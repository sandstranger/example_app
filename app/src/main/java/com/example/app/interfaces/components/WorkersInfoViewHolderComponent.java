package com.example.app.interfaces.components;

import com.example.app.modules.WorkersInfoViewHolderModule;
import com.example.app.view.viewholder.WorkersInfoViewHolder;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = WorkersInfoViewHolderModule.class)
@Singleton
public interface WorkersInfoViewHolderComponent {
    void inject (WorkersInfoViewHolder viewHolder);
}
