package com.example.app.interfaces.components;

import com.example.app.modules.MainActivityModule;
import com.example.app.view.activity.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = MainActivityModule.class)
@Singleton
public interface MainActivityComponent {
    void inject(MainActivity mainActivity);
}
