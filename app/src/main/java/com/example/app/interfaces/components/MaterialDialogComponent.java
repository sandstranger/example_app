package com.example.app.interfaces.components;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.app.modules.MaterialDialogModule;

import dagger.Component;

@Component(modules = MaterialDialogModule.class)
public interface MaterialDialogComponent {
    MaterialDialog getMaterialDialog();
}
