package com.example.app.interfaces.listeners;

import com.example.app.model.Worker;

import java.util.List;

public interface WorkersClientListener<T> {
    void onResponce (T results);
    void onFailed ();
}
