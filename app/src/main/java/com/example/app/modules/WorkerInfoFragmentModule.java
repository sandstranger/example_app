package com.example.app.modules;

import android.view.View;

import com.example.app.view.viewholder.WorkerInfoViewHolder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {RootViewModule.class, FragmentsControllerProviderModule.class})
public class WorkerInfoFragmentModule {

    @Provides
    @Singleton
    public WorkerInfoViewHolder viewHolder(View rootView) {
        return new WorkerInfoViewHolder(rootView);
    }

}
