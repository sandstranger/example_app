package com.example.app.modules;


import android.content.Context;

import com.example.app.interfaces.qualifiers.ActivityContext;

import dagger.Module;
import dagger.Provides;
import lombok.NonNull;

@Module
public class ContextModule {
    @NonNull
    @ActivityContext
    private final Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @ActivityContext
    public Context context() {
        return context;
    }
}
