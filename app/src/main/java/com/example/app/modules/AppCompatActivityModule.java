package com.example.app.modules;

import android.support.v7.app.AppCompatActivity;

import com.example.app.interfaces.qualifiers.AppCompatActivityProvider;
import com.example.app.view.activity.MainActivity;

import dagger.Module;
import dagger.Provides;
import lombok.NonNull;

@Module
public class AppCompatActivityModule {
    @NonNull
    @AppCompatActivityProvider
    private final MainActivity activity;

    public AppCompatActivityModule(MainActivity activity) {
        this.activity = activity;
    }

    @Provides
    @AppCompatActivityProvider
    public MainActivity activity() {
        return activity;
    }

}
