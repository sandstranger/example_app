package com.example.app.modules;

import android.view.View;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RootViewModule {
    private final View view;

    public RootViewModule(View view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public View rootView() {
        return view;
    }
}
