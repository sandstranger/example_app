package com.example.app.modules;

import android.view.View;

import com.example.app.view.viewholder.SpecialtyViewHolder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {RootViewModule.class,FragmentsControllerProviderModule.class})
public class SpecialtyFragmentModule {
    @Provides
    @Singleton
    public SpecialtyViewHolder viewHolder(View rootView) {
        return new SpecialtyViewHolder(rootView);
    }
}
