package com.example.app.modules;

import com.example.app.interfaces.qualifiers.AppCompatActivityProvider;
import com.example.app.view.activity.MainActivity;
import com.example.app.view.controller.FragmentsController;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = AppCompatActivityModule.class)
public class FragmentsControllerProviderModule {

    @Provides
    @Singleton
    public FragmentsController fragmentsController(@AppCompatActivityProvider MainActivity activity) {
        return new FragmentsController(activity);
    }
}
