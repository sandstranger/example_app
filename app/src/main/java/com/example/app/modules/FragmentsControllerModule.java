package com.example.app.modules;

import com.example.app.view.fragment.SpecialtyFragment;
import com.example.app.view.fragment.WorkerInfoFragment;
import com.example.app.view.fragment.WorkersInfoFragment;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentsControllerModule {

    @Provides
    public static WorkersInfoFragment workersInfoFragment() {
        return new WorkersInfoFragment();
    }

    @Provides
    public static WorkerInfoFragment workerInfoFragment() {
        return new WorkerInfoFragment();
    }

    @Provides
    public static SpecialtyFragment specialtyFragment() {
        return new SpecialtyFragment();
    }
}
