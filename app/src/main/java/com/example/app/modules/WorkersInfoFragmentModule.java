package com.example.app.modules;


import android.view.View;

import com.example.app.view.viewholder.WorkersInfoViewHolder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {RootViewModule.class,FragmentsControllerProviderModule.class})
public class WorkersInfoFragmentModule {
    @Provides
    @Singleton
    public WorkersInfoViewHolder viewHolder(View rootView) {
        return new WorkersInfoViewHolder(rootView);
    }

}
