package com.example.app.modules;

import com.example.app.view.adapter.WorkersInfoAdapter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class WorkersInfoViewHolderModule {
    @Provides
    @Singleton
    public WorkersInfoAdapter adapter() {
        return new WorkersInfoAdapter();
    }
}
