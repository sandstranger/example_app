package com.example.app.modules;

import com.example.app.view.adapter.SpecialtyItemsAdapter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SpecialtyViewHolderModule {

    @Provides
    @Singleton
    public SpecialtyItemsAdapter adapter (){
        return new SpecialtyItemsAdapter();
    }
}
