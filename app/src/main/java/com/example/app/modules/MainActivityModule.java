package com.example.app.modules;

import android.content.Context;

import com.example.app.client.WorkersClient;
import com.example.app.interfaces.qualifiers.ActivityContext;
import com.example.app.view.viewholder.MainViewHolder;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ContextModule.class,FragmentsControllerProviderModule.class})
public class MainActivityModule {

    @Provides
    WorkersClient workersClient() {
        return new WorkersClient();
    }

    @Provides
    MainViewHolder viewHolder(@ActivityContext Context context) {
        return new MainViewHolder(context);
    }
}
