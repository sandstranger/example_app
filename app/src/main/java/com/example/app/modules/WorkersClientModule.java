package com.example.app.modules;

import com.example.app.interfaces.api.WorkersApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.app.utils.Constants.API_BASE_URL;

@Module (includes = OkHttpClientModule.class)
public class WorkersClientModule {

    @Provides
    @Singleton
    public static WorkersApi workersApi(Retrofit retrofit) {
        return retrofit.create(WorkersApi.class);
    }

    @Provides
    @Singleton
    public static Retrofit retrofit(OkHttpClient okHttpClient) {
        String baseUrl = API_BASE_URL;
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
