package com.example.app.modules;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.app.R;
import com.example.app.interfaces.qualifiers.ActivityContext;

import dagger.Module;
import dagger.Provides;

@Module(includes = ContextModule.class)
public class MaterialDialogModule {

    @Provides
    public MaterialDialog dialog(@ActivityContext Context context) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context).
                cancelable(true).positiveText(R.string.ok_btn_title);
        return builder.build();
    }
}
