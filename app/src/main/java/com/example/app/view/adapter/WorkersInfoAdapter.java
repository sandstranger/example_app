package com.example.app.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.interfaces.listeners.Callback;
import com.example.app.model.Worker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Setter;

public class WorkersInfoAdapter extends RecyclerView.Adapter<WorkersInfoAdapter.ViewHolder> {
    private List<Worker> workersList = new ArrayList<>();
    @Setter
    private Callback callback;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.worker_info_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.bindView(workersList.get(position));
        viewHolder.setCallback(callback);
    }

    public void setItems(List<Worker> workersList) {
        this.workersList = workersList;
        notifyDataSetChanged();
    }

    public void clear() {
        if (workersList != null) {
            workersList.clear();
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return workersList != null ? workersList.size() : 0;
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private Worker dataModel;
        @BindView(R.id.workerNameTextView)
        protected TextView workerNameTextView;
        @BindView(R.id.workerAgeTextView)
        protected TextView workerAgeTextView;
        @BindView(R.id.workerSurNameTextView)
        protected TextView workerSurNameTextView;
        @Setter
        private Callback callback;

        protected ViewHolder(View rootView) {
            super(rootView);
            ButterKnife.bind(this,rootView);
        }

        protected void bindView(Worker dataModel) {
            this.dataModel = dataModel;
            workerNameTextView.setText(dataModel.getWorkerName());
            workerSurNameTextView.setText(dataModel.getWorkerSurName());
            workerAgeTextView.setText(dataModel.getWorkerAge());
        }

        @OnClick(R.id.workerInfoRootLayout)
        protected void onWorkerInfoRootLayoutClicked() {
            if (callback!=null){
                callback.callAction(dataModel);
            }
        }
    }
}
