package com.example.app.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.app.R;

abstract class BaseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResourceId(), container, false);
        configureDagger(view);
        bindViewHolder();
        return view;
    }

    protected abstract void configureDagger(View view);

    protected abstract void bindViewHolder();

    protected int getLayoutResourceId() {
        return R.layout.recycler_view_fragment_layout;
    }
}
