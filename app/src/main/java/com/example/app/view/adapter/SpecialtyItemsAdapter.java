package com.example.app.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.interfaces.listeners.Callback;
import com.example.app.model.WorkerSpeciality;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.NonNull;
import lombok.Setter;

public class SpecialtyItemsAdapter extends Adapter<SpecialtyItemsAdapter.ViewHolder> {
    private List<WorkerSpeciality> workerSpecialities = new ArrayList<>();
    @NonNull
    @Setter
    private Callback callback;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.specialty_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(rootView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.bindView(workerSpecialities.get(position));
        viewHolder.setCallback(callback);
    }

    @Override
    public int getItemCount() {
        return workerSpecialities != null ? workerSpecialities.size() : 0;
    }

    public void setItems(List<WorkerSpeciality> workerSpecialities) {
        this.workerSpecialities = workerSpecialities;
        notifyDataSetChanged();
    }

    public void clear() {
        if (workerSpecialities != null) {
            workerSpecialities.clear();
        }
        notifyDataSetChanged();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder {
        private WorkerSpeciality dataModel;
        @BindView(R.id.specialtyNameTextView)
        protected TextView specialtyNameTextView;
        @NonNull
        @Setter
        private Callback callback;

        public ViewHolder(View rootView) {
            super(rootView);
            ButterKnife.bind(this, rootView);
        }

        private void bindView(WorkerSpeciality dataModel) {
            this.dataModel = dataModel;
            specialtyNameTextView.setText(this.dataModel.getSpecialityName());
        }

        @OnClick(R.id.specialtyItemBtn)
        protected void onSpecialtyItemBtnClicked() {
            if (callback != null) {
                callback.callAction(dataModel);
            }
        }
    }

}
