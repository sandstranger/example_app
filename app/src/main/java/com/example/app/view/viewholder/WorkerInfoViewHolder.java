package com.example.app.view.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.model.Worker;
import com.example.app.utils.extensions.ImageViewExtensions;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.experimental.ExtensionMethod;

@ExtensionMethod(ImageViewExtensions.class)
public class WorkerInfoViewHolder {

    @BindView(R.id.imageView)
    protected ImageView imageView;
    @BindView(R.id.workerNameTextView)
    protected TextView workerNameTextView;
    @BindView(R.id.workerAgeTextView)
    protected TextView workerAgeTextView;
    @BindView(R.id.workerBirthdayTextView)
    protected TextView workerBirthdayTextView;
    @BindView(R.id.workerSurNameTextView)
    protected TextView workerSurNameTextView;
    @BindView(R.id.workerSpecialityTextView)
    protected TextView workerSpecialityTextView;

    public WorkerInfoViewHolder(View rootView) {
        ButterKnife.bind(this, rootView);
    }

    public void bindView(Worker dataModel) {
        imageView.loadImage(dataModel.getWorkerAvatarImageLink());
        workerNameTextView.setText(dataModel.getWorkerName());
        workerSurNameTextView.setText(dataModel.getWorkerSurName());
        workerAgeTextView.setText(dataModel.getWorkerAge());
        workerBirthdayTextView.setText(dataModel.getWorkerBirthday());
        workerSpecialityTextView.setText(dataModel.getWorkerSpecialityName());
    }
}
