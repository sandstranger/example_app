package com.example.app.view.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.app.R;
import com.example.app.client.SugarORMClient;
import com.example.app.client.WorkersClient;
import com.example.app.interfaces.components.DaggerMainActivityComponent;
import com.example.app.interfaces.listeners.Callback;
import com.example.app.interfaces.listeners.OnBackPressed;
import com.example.app.interfaces.listeners.WorkersClientListener;
import com.example.app.model.Worker;
import com.example.app.modules.AppCompatActivityModule;
import com.example.app.modules.ContextModule;
import com.example.app.view.controller.FragmentsController;
import com.example.app.view.viewholder.MainViewHolder;

import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity{
    @Inject
    protected MainViewHolder viewHolder;
    @Inject
    protected WorkersClient workersClient;
    @Inject
    protected FragmentsController fragmentsController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buildDagger();
        setListenerToWorkersClient();
        viewHolder.setConnectToServerListener(new Callback() {
            @Override
            public void callAction() {
                enqueue();
            }
        });
        enqueue();
    }

    private void setListenerToWorkersClient() {
        workersClient.setConnectionListener(new WorkersClientListener<List<Worker>>() {
            @Override
            public void onResponce(List<Worker> workersList) {
                SugarORMClient.commitTransaction(workersList);
                viewHolder.hideProgressDialog();
                addMainFragment();
            }

            @Override
            public void onFailed() {
                viewHolder.hideProgressDialog();
                viewHolder.showConnectionFailedDialog();
            }
        });
    }

    private void buildDagger() {
        DaggerMainActivityComponent.builder().contextModule
                (new ContextModule(MainActivity.this))
                .appCompatActivityModule(new AppCompatActivityModule(this))
                .build().inject(MainActivity.this);
    }

    private void enqueue() {
        if (!SugarORMClient.isEmpty()) {
            addMainFragment();
            return;
        }
        workersClient.enqueue();
        viewHolder.showProgressDialog();
    }

    private void addMainFragment() {
        viewHolder.getConnectToServerBtn().setVisibility(View.GONE);
        fragmentsController.pushFragment(FragmentsController.FragmentsTypes.SPECIALTY_FRAGMENT);
    }

    @Override
    public void onBackPressed() {
        int fragmentPosition = 0;
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getFragments().size()>0) {
            Fragment currentFragment = fragmentManager.getFragments().get(fragmentPosition);
            if (currentFragment != null && currentFragment instanceof OnBackPressed) {
                ((OnBackPressed) currentFragment).onBackPressed();
                return;
            }
        }
        super.onBackPressed();
    }
}
