package com.example.app.view.viewholder;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.app.R;
import com.example.app.interfaces.listeners.Callback;
import com.example.app.utils.extensions.RecyclerViewExtensions;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.NonNull;
import lombok.experimental.ExtensionMethod;

@ExtensionMethod(RecyclerViewExtensions.class)
abstract class BaseFragmentView<T> {
    @NonNull
    @BindView(R.id.recyclerView)
    protected RecyclerView recyclerView;

    public BaseFragmentView(View rootView) {
        configureDagger();
        ButterKnife.bind(this, rootView);
        recyclerView.setAdapter(getAdapter());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(rootView.getContext()));
        recyclerView.addDivider();
    }

    protected abstract void configureDagger();

    protected abstract RecyclerView.Adapter getAdapter();

    public abstract void bindView(T data);

    public abstract void setCallback(Callback callback);
}
