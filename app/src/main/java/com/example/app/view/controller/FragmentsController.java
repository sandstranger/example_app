package com.example.app.view.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.app.R;
import com.example.app.interfaces.components.DaggerFragmentsControllerComponent;
import com.example.app.interfaces.components.FragmentsControllerComponent;
import com.example.app.view.activity.MainActivity;

import lombok.Getter;

public class FragmentsController {
    public enum FragmentsTypes {
        SPECIALTY_FRAGMENT,
        WORKERS_INFO_FRAGMENT,
        WORKER_INFO_FRAGMENT
    }

    @Getter
    private FragmentsControllerComponent fragmentsControllerComponent;
    private AppCompatActivity activity;

    public FragmentsController(MainActivity activity) {
        this.activity = activity;
        fragmentsControllerComponent = DaggerFragmentsControllerComponent.builder().build();
    }

    public void pushFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentTransaction();
        transaction.replace(R.id.fragmentsContentFrameLayout, fragment);
        transaction.commit();
    }

    public void pushFragment(FragmentsTypes fragmentType) {
        FragmentTransaction transaction = getFragmentTransaction();
        transaction.replace(R.id.fragmentsContentFrameLayout, getFragment(fragmentType));
        transaction.commit();
    }

    private Fragment getFragment(FragmentsTypes fragmentType) {
        switch (fragmentType) {
            case SPECIALTY_FRAGMENT:
                return fragmentsControllerComponent.getSpecialtyFragment();
            case WORKER_INFO_FRAGMENT:
                return fragmentsControllerComponent.getWorkerInfoFragment();
            case WORKERS_INFO_FRAGMENT:
                return fragmentsControllerComponent.getWorkersInfoFragment();
            default:
                return null;
        }
    }

    private FragmentTransaction getFragmentTransaction() {
        return activity.getSupportFragmentManager().beginTransaction();
    }
}
