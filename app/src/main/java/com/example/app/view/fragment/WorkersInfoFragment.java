package com.example.app.view.fragment;

import android.os.Bundle;
import android.view.View;

import com.example.app.interfaces.components.DaggerWorkersInfoFragmentComponent;
import com.example.app.interfaces.listeners.Callback;
import com.example.app.interfaces.listeners.OnBackPressed;
import com.example.app.model.Worker;
import com.example.app.model.WorkerSpeciality;
import com.example.app.modules.AppCompatActivityModule;
import com.example.app.modules.RootViewModule;
import com.example.app.view.activity.MainActivity;
import com.example.app.view.controller.FragmentsController;
import com.example.app.view.viewholder.WorkersInfoViewHolder;

import javax.inject.Inject;

import static com.example.app.utils.BundleUtils.getArgs;
import static com.example.app.utils.Constants.WORKERS_SPECIALTY_BUNDLE_DATA_TAG;

public class WorkersInfoFragment extends BaseFragment implements OnBackPressed {
    @Inject
    protected WorkersInfoViewHolder viewHolder;
    @Inject
    protected FragmentsController fragmentsController;

    @Override
    protected void configureDagger(View view) {
        MainActivity activity = (MainActivity) getActivity();
        DaggerWorkersInfoFragmentComponent.builder()
                .appCompatActivityModule(new AppCompatActivityModule(activity))
                .rootViewModule(new RootViewModule(view))
                .build().inject(this);
    }

    @Override
    protected void bindViewHolder() {
        viewHolder.setCallback(new Callback<Worker>() {
            @Override
            public void callAction(Worker worker) {
                pushWorkerInfoFragment(worker);
            }
        });
        viewHolder.bindView(getSavedWorkersSpecialty().getWorkers());
    }

    @Override
    public void onBackPressed() {
        fragmentsController.pushFragment(FragmentsController.FragmentsTypes.SPECIALTY_FRAGMENT);
    }

    private void pushWorkerInfoFragment(Worker worker) {
        WorkerInfoFragment workerInfoFragment = fragmentsController.
                getFragmentsControllerComponent().getWorkerInfoFragment();
        workerInfoFragment.setArguments(getArgs(worker));
        fragmentsController.pushFragment(workerInfoFragment);
    }

    private WorkerSpeciality getSavedWorkersSpecialty() {
        WorkerSpeciality workersSpecialty = new WorkerSpeciality();
        Bundle bundle = getArguments();
        if (bundle != null) {
            workersSpecialty = (WorkerSpeciality) bundle.getSerializable(WORKERS_SPECIALTY_BUNDLE_DATA_TAG);
        }
        return workersSpecialty;
    }
}
