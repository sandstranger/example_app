package com.example.app.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.app.interfaces.listeners.Callback;
import com.example.app.interfaces.components.DaggerWorkersInfoViewHolderComponent;
import com.example.app.model.Worker;
import com.example.app.view.adapter.WorkersInfoAdapter;

import java.util.List;

import javax.inject.Inject;

public class WorkersInfoViewHolder extends BaseFragmentView<List<Worker>> {
    @Inject
    protected WorkersInfoAdapter adapter;

    public WorkersInfoViewHolder(View rootView) {
        super(rootView);
    }

    @Override
    protected void configureDagger() {
        DaggerWorkersInfoViewHolderComponent.builder().build().inject(this);
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    @Override
    public void bindView(List<Worker> workersList) {
        adapter.setItems(workersList);
    }

    @Override
    public void setCallback(Callback callback) {
        adapter.setCallback(callback);
    }
}
