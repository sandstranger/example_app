package com.example.app.view.fragment;

import android.view.View;

import com.example.app.client.SugarORMClient;
import com.example.app.interfaces.components.DaggerSpecialtyFragmentComponent;
import com.example.app.interfaces.listeners.Callback;
import com.example.app.model.WorkerSpeciality;
import com.example.app.modules.AppCompatActivityModule;
import com.example.app.modules.RootViewModule;
import com.example.app.view.activity.MainActivity;
import com.example.app.view.controller.FragmentsController;
import com.example.app.view.viewholder.SpecialtyViewHolder;

import javax.inject.Inject;

import static com.example.app.utils.BundleUtils.getArgs;

public class SpecialtyFragment extends BaseFragment {

    @Inject
    protected SpecialtyViewHolder viewHolder;
    @Inject
    protected FragmentsController fragmentsController;

    @Override
    protected void configureDagger(View view) {
        MainActivity activity = (MainActivity) getActivity();
        DaggerSpecialtyFragmentComponent.builder()
                .appCompatActivityModule(new AppCompatActivityModule(activity))
                .rootViewModule(new RootViewModule(view))
                .build().inject(this);
    }

    @Override
    protected void bindViewHolder() {
        viewHolder.bindView(SugarORMClient.getData(WorkerSpeciality.class));
        viewHolder.setCallback(new Callback<WorkerSpeciality>() {
            @Override
            public void callAction(WorkerSpeciality workersSpecialty) {
                pushWorkersInfoFragment(workersSpecialty);
            }
        });
    }

    private void pushWorkersInfoFragment(WorkerSpeciality workersSpecialty) {
        WorkersInfoFragment workersInfoFragment = fragmentsController.getFragmentsControllerComponent()
                .getWorkersInfoFragment();
        workersInfoFragment.setArguments(getArgs(workersSpecialty));
        fragmentsController.pushFragment(workersInfoFragment);
    }
}
