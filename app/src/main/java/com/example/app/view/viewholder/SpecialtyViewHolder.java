package com.example.app.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.app.interfaces.listeners.Callback;
import com.example.app.interfaces.components.DaggerSpecialtyViewHolderComponent;
import com.example.app.model.WorkerSpeciality;
import com.example.app.view.adapter.SpecialtyItemsAdapter;

import java.util.List;

import javax.inject.Inject;

public class SpecialtyViewHolder extends BaseFragmentView<List<WorkerSpeciality>> {
    @Inject
    protected SpecialtyItemsAdapter adapter;

    public SpecialtyViewHolder(View rootView) {
        super(rootView);
    }

    @Override
    protected void configureDagger() {
        DaggerSpecialtyViewHolderComponent.builder().build().inject(this);
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    @Override
    public void bindView(List<WorkerSpeciality> workerSpecialities) {
        adapter.setItems(workerSpecialities);
    }

    @Override
    public void setCallback(Callback callback) {
        adapter.setCallback(callback);
    }
}
