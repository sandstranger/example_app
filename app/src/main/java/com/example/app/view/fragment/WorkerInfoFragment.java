package com.example.app.view.fragment;

import android.os.Bundle;
import android.view.View;

import com.example.app.R;
import com.example.app.interfaces.components.DaggerWorkerInfoFragmentComponent;
import com.example.app.interfaces.listeners.OnBackPressed;
import com.example.app.model.Worker;
import com.example.app.modules.AppCompatActivityModule;
import com.example.app.modules.RootViewModule;
import com.example.app.view.activity.MainActivity;
import com.example.app.view.controller.FragmentsController;
import com.example.app.view.viewholder.WorkerInfoViewHolder;

import javax.inject.Inject;

import static com.example.app.utils.BundleUtils.getArgs;
import static com.example.app.utils.Constants.WORKER_BUNDLE_DATA_TAG;

public class WorkerInfoFragment extends BaseFragment implements OnBackPressed {
    private Worker worker = new Worker();
    @Inject
    protected FragmentsController fragmentsController;
    @Inject
    protected WorkerInfoViewHolder viewHolder;

    @Override
    protected void configureDagger(View view) {
        MainActivity activity = (MainActivity) getActivity();
        DaggerWorkerInfoFragmentComponent.builder().
                appCompatActivityModule(new AppCompatActivityModule(activity))
                .rootViewModule(new RootViewModule(view))
                .build().inject(this);
    }

    @Override
    protected void bindViewHolder() {
        worker = getSavedWorker();
        viewHolder.bindView(worker);
    }

    @Override
    public void onBackPressed() {
        pushFragment();
    }

    private void pushFragment() {
        WorkersInfoFragment workersInfoFragment = fragmentsController.getFragmentsControllerComponent().getWorkersInfoFragment();
        workersInfoFragment.setArguments(getArgs(worker.getWorkerSpecality()));
        fragmentsController.pushFragment(workersInfoFragment);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.worker_info_fragment_layout;
    }

    private Worker getSavedWorker() {
        Worker worker = new Worker();
        Bundle bundle = getArguments();
        if (bundle != null) {
            worker = (Worker) bundle.getSerializable(WORKER_BUNDLE_DATA_TAG);
        }
        return worker;
    }

}
