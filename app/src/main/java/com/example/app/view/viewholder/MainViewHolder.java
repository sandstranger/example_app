package com.example.app.view.viewholder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.app.R;
import com.example.app.interfaces.components.DaggerMaterialDialogComponent;
import com.example.app.interfaces.components.MaterialDialogComponent;
import com.example.app.interfaces.listeners.Callback;
import com.example.app.modules.ContextModule;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Getter;
import lombok.NonNull;

public class MainViewHolder {
    @Getter
    @BindView(R.id.connectToServerBtn)
    protected Button connectToServerBtn;
    @BindString(R.string.progress_dialog_message)
    protected String progressDialogMessage;
    @NonNull
    private ProgressDialog progressDialog;
    @NonNull
    private Context mContext;
    private MaterialDialogComponent materialDialogComponent;
    private Callback callBack;

    public MainViewHolder(Context context) {
        mContext = context;
        ButterKnife.bind(this, (Activity) context);
        materialDialogComponent = DaggerMaterialDialogComponent.builder()
                .contextModule(new ContextModule(mContext)).build();
    }

    public void showProgressDialog() {
        progressDialog = ProgressDialog.show(mContext, "", progressDialogMessage);
    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void showConnectionFailedDialog() {
        MaterialDialog dialog = materialDialogComponent.getMaterialDialog();
        dialog.setContent(R.string.connection_failed_message);
        dialog.show();
    }

    @OnClick(R.id.connectToServerBtn)
    protected void connectToServer() {
        if (callBack != null) {
            callBack.callAction();
        }
    }

    public void setConnectToServerListener(Callback listener) {
        callBack = listener;
    }
}
