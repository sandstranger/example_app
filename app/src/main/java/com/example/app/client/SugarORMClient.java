package com.example.app.client;

import android.content.Context;

import com.example.app.model.Worker;
import com.example.app.utils.DebugLogger;
import com.orm.SugarContext;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.List;

import lombok.val;

public final class SugarORMClient {
    private static boolean sugarInit = false;

    private SugarORMClient() {

    }

    public static void initSugar(Context context) {
        if (sugarInit) {
            return;
        }
        SugarContext.init(context);
        sugarInit = true;
    }

    public static void terminateSugar() {
        SugarContext.terminate();
    }

    public static void commitTransaction(List<Worker> workersList) {
        for (val worker : workersList) {
            val workerSpeciality = worker.getWorkerSpecialities().get(0);
            workerSpeciality.save();
            worker.setWorkerSpecialityName(workerSpeciality.getSpecialityName());
            worker.save();
        }
    }

    public static <E extends SugarRecord> List<E> getData(Class<E> clazz) {
        return SugarRecord.listAll(clazz);
    }

    public static <E extends SugarRecord> List<E> getData(Class<E> clazz, String queryKey, String queryValue) {
        List<E> records = new ArrayList<E>();
        try {
            records = SugarRecord.find(clazz, queryKey + " = ? ", queryValue);
        } catch (Exception e) {
            DebugLogger.logException(e);
        }
        return records;
    }

    public static boolean isEmpty() {
        boolean empty = true;
        try {
            List<Worker> records = Worker.listAll(Worker.class);
            empty = records == null || records.size() <= 0;
        } catch (Exception e) {
            DebugLogger.logException(e);
        }
        return empty;
    }
}
