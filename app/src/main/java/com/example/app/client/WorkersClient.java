package com.example.app.client;

import com.example.app.interfaces.api.WorkersApi;
import com.example.app.interfaces.components.DaggerWorkersClientComponent;
import com.example.app.interfaces.listeners.WorkersClientListener;
import com.example.app.model.WorkersList;
import com.example.app.utils.DebugLogger;

import javax.inject.Inject;

import lombok.val;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkersClient {
    private final static String CONNECTION_SUCSESS_MESSAGE = "CONNECTION SUCSESS";
    private final static String CONNECTION_FAILED_MESSAGE = "CONNECTION FAILED";
    @Inject
    protected WorkersApi workersApi;
    private WorkersClientListener connectionListener;

    public WorkersClient() {
        DaggerWorkersClientComponent.builder().build().inject(this);
    }

    public void enqueue() {
        val workersCall = workersApi.getWorkers();
        DebugLogger.logMessage("CONNECTION URL = " + workersCall.request().toString());
        workersCall.enqueue(new Callback<WorkersList>() {
            @Override
            public void onResponse(Call<WorkersList> call, Response<WorkersList> response) {
                DebugLogger.logMessage(CONNECTION_SUCSESS_MESSAGE);
                if (connectionListener != null) {
                    connectionListener.onResponce(response.body().getWorkersList());
                }
            }

            @Override
            public void onFailure(Call<WorkersList> call, Throwable t) {
                DebugLogger.logMessage(CONNECTION_FAILED_MESSAGE + " " + t.getMessage());
                if (connectionListener != null) {
                    connectionListener.onFailed();
                }
            }
        });
    }

    public void setConnectionListener(WorkersClientListener listener) {
        if (listener != null) {
            connectionListener = listener;
        }
    }
}
