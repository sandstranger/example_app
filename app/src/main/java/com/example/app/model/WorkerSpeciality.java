package com.example.app.model;

import com.example.app.client.SugarORMClient;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.ToString;

@ToString
public class WorkerSpeciality extends SugarRecord implements Serializable {

    @Getter
    @SerializedName("specialty_id")
    private int specialityId = 0;

    @Unique
    @Getter
    @SerializedName("name")
    private String specialityName = "";

    public WorkerSpeciality() {

    }

    public List<Worker> getWorkers() {
        return SugarORMClient.getData(Worker.class, "worker_Speciality_Name", specialityName);
    }

}
