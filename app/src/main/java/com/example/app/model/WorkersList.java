package com.example.app.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class WorkersList {
    @Getter
    @SerializedName("response")
    private List<Worker> workersList = new ArrayList<>();

    public WorkersList (){

    }
}
