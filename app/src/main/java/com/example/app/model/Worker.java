package com.example.app.model;

import com.example.app.client.SugarORMClient;
import com.example.app.utils.date.DateFormatter;
import com.example.app.utils.date.DateHelper;
import com.example.app.utils.extensions.StringExtensions;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.ExtensionMethod;

@ExtensionMethod(StringExtensions.class)
@ToString
public class Worker extends SugarRecord implements Serializable {
    private final static String DEFAULT_WORKER_BIRTHDAY_MARK = "-";

    @SerializedName("f_name")
    private String workerName = "";
    @SerializedName("l_name")
    private String workerSurName = "";
    @SerializedName("birthday")
    private String workerBirthday = "";
    @Getter
    @SerializedName("avatr_url")
    private String workerAvatarImageLink = "";
    @Getter
    @SerializedName("specialty")
    private List<WorkerSpeciality> workerSpecialities = new ArrayList<>();
    @Expose
    @Getter
    @Setter
    private String workerSpecialityName;

    public Worker() {

    }

    public String getWorkerName() {
        return workerName.capitalize();
    }

    public String getWorkerSurName() {
        return workerSurName.capitalize();
    }

    public String getWorkerBirthday() {
        return workerBirthday == null || workerBirthday.isEmpty() ?
                DEFAULT_WORKER_BIRTHDAY_MARK : DateFormatter.formateDateStringToDefault(workerBirthday);
    }

    public String getWorkerAge() {
        int age = DateHelper.yearsDifferenceBetweenToday(workerBirthday);
        return age == 0 ? DEFAULT_WORKER_BIRTHDAY_MARK : String.valueOf(age);
    }

    public WorkerSpeciality getWorkerSpecality() {
        List<WorkerSpeciality> workerSpecialities = SugarORMClient
                .getData(WorkerSpeciality.class, "speciality_Name", workerSpecialityName);
        return workerSpecialities != null && workerSpecialities.size() > 0 ? workerSpecialities.get(0) : new WorkerSpeciality();
    }
}
