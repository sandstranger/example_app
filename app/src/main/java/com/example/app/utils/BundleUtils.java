package com.example.app.utils;

import android.os.Bundle;

import com.example.app.model.Worker;
import com.example.app.model.WorkerSpeciality;

import static com.example.app.utils.Constants.WORKERS_SPECIALTY_BUNDLE_DATA_TAG;
import static com.example.app.utils.Constants.WORKER_BUNDLE_DATA_TAG;

public final class BundleUtils {

    private BundleUtils() {

    }

    public static Bundle getArgs(WorkerSpeciality workersSpecialty) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(WORKERS_SPECIALTY_BUNDLE_DATA_TAG, workersSpecialty);
        return bundle;
    }

    public static Bundle getArgs(Worker worker) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(WORKER_BUNDLE_DATA_TAG, worker);
        return bundle;
    }
}
