package com.example.app.utils.extensions;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageViewExtensions {
    public static void loadImage(ImageView imageView, String imageLink) {
        if (imageLink==null || imageLink.isEmpty() || imageView == null){
            return;
        }
        Picasso picasso = Picasso.get();
        picasso.load(imageLink).into(imageView);
    }
}
