package com.example.app.utils;

import android.util.Log;

import com.example.app.BuildConfig;

public final class DebugLogger {
    private static final String APP_TAG = "example_app";
    private static final String EXCEPTION_TAG = "exception";

    private DebugLogger(){

    }

    public static void logMessage(String message) {
        if (debugBuild()) {
            Log.d(APP_TAG, message);
        }
    }

    public static void logException(Exception e) {
        if (debugBuild()) {
            Log.e(APP_TAG, EXCEPTION_TAG, e);
        }
    }

    private static boolean debugBuild() {
        return BuildConfig.DEBUG;
    }
}
