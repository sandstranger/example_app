package com.example.app.utils.extensions;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;

public class RecyclerViewExtensions {
    public static void addDivider(RecyclerView recyclerView) {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
    }
}
