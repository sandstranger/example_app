package com.example.app.utils.extensions;

import org.apache.commons.lang3.StringUtils;

public class StringExtensions {

    public static  String capitalize(String in) {
        return in.isEmpty() ? in : StringUtils.capitalize(in.toLowerCase());
    }
}
