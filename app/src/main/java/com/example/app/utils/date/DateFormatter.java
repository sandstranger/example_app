package com.example.app.utils.date;

import com.example.app.utils.DebugLogger;

import net.balusc.util.DateUtil;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

public final class DateFormatter {
    private static final String DEFAULT_DATE_FORMAT = "dd-MM-yyyy";

    private DateFormatter() {

    }

    public static String formateDateStringToDefault(String dateString) {
        String formattedDateString = dateString;
        try {
            Date date = DateUtil.parse(dateString);
            formattedDateString = DateFormatUtils.format(date, DEFAULT_DATE_FORMAT);
        } catch (Exception e) {
            DebugLogger.logException(e);
        }
        return formattedDateString;
    }
}
