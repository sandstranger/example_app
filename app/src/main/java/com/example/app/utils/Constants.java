package com.example.app.utils;

public final class Constants {

    private Constants() {
    }

    public static final String WORKER_BUNDLE_DATA_TAG = "worker_data";
    public static final String WORKERS_SPECIALTY_BUNDLE_DATA_TAG = "workers_specialty_data";
    public static final String WORKERS_JSON_FILE_NAME = "testTask.json";
    public static final String API_BASE_URL = "http://gitlab.65apps.com/65gb/static/raw/master/";
}
