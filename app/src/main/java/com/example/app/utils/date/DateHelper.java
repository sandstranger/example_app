package com.example.app.utils.date;

import com.example.app.utils.DebugLogger;

import net.balusc.util.DateUtil;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Calendar;
import java.util.Date;

public final class DateHelper {
    private static final int START_COUNT_YEAR = 1970;

    private DateHelper() {

    }

    public static int yearsDifferenceBetweenToday(String targetDateString) {
        int yearsDifference = 0;
        try {
            Calendar c = Calendar.getInstance();
            Date currentDate = new Date();
            Date targetDate = DateUtil.parse(targetDateString);
            DebugLogger.logMessage(targetDate.toString());
            c.setTimeInMillis(currentDate.getTime() - targetDate.getTime());
            yearsDifference = c.get(Calendar.YEAR) - START_COUNT_YEAR;
        } catch (Exception e) {
            DebugLogger.logException(e);
        }
        return yearsDifference;
    }
}
